//
//  RegisterRouterImpl.swift
//  OTP
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation
import UIKit
import Core

public class OTPRouterImpl {
    
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "id.mandiri.dip.OTP")
        let vc = OTPViewController(nibName: "OTPViewController", bundle: bundle)
        let networkManager = AuthNetworkManagerImpl()
        let router = OTPRouterImpl()
        let interactor = OTPInteractorImpl(networkManager: networkManager)
        let presenter = OTPPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension OTPRouterImpl: OTPRouter {
    func navigateToHome() {
        NotificationCenter.default.post(name: Notification.Name("reloadRootView"), object: nil)
    }
    func navigateToLogin() {
        AppRouter.shared.navigateLogin()
    }
}
