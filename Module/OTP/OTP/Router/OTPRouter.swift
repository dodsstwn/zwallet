//
//  RegisterRouter.swift
//  OTP
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation

protocol OTPRouter {
    func navigateToHome()
    func navigateToLogin()
}
