//
//  OTPPresenterImpl.swift
//  OTP
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation

class OTPPresenterImpl: OTPPresenter {
    
    
    let view: OTPView
    let interactor: OTPInteractor
    let router: OTPRouter
    
    init(view: OTPView, interactor: OTPInteractor, router: OTPRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func otp(email: String, otp: String) {
        self.interactor.postOTPData(email: email, otp: otp)
    }
    
    func backToLogin() {
        self.router.navigateToLogin()
    }
}

extension OTPPresenterImpl: OTPInteractorOutput {
    func authenticationResult(isSuccess: Bool) {
        if isSuccess {
            self.router.navigateToHome()
        } else {
            self.view.showError()
        }
    }
}
