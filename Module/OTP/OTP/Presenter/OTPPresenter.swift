//
//  OTPPresenter.swift
//  OTP
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation

protocol OTPPresenter {
    func otp(email: String, otp: String)
    func backToLogin()
}
