//
//  OTPInteractorImpl.swift
//  OTP
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation
import Core

class OTPInteractorImpl: OTPInteractor {
    
    
    var interactorOutput: OTPInteractorOutput?
    var authNetworkManager: AuthNetworkManager
    
    init(networkManager: AuthNetworkManager) {
        self.authNetworkManager = networkManager
    }
    
    func postOTPData(email: String, otp: String) {
        self.authNetworkManager.otp(email: email, otp: otp) { data, error in
            if let otpData = data {
                self.interactorOutput?.authenticationResult(isSuccess: true)
            } else {
                self.interactorOutput?.authenticationResult(isSuccess: false)
            }
        }
    }
    
}
