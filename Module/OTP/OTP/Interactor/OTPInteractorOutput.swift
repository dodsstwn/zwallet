//
//  OTPInteractorOutput.swift
//  OTP
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation

protocol OTPInteractorOutput {
    func authenticationResult(isSuccess: Bool)
}
