//
//  RegisterInteractor.swift
//  OTP
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation

protocol OTPInteractor {
    func postOTPData(email: String, otp: String)
}
