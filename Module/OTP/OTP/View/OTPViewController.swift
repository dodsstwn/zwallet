//
//  OTPViewController.swift
//  OTP
//
//  Created by Dodsstwn on 27/05/21.
//

import UIKit
import Core
import Register

class OTPViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var firstInp: UITextField!
    @IBOutlet weak var secondInp: UITextField!
    @IBOutlet weak var thirdInp: UITextField!
    @IBOutlet weak var fourthInp: UITextField!
    @IBOutlet weak var fifthInp: UITextField!
    @IBOutlet weak var sixInp: UITextField!
    
    @IBAction func confirmButton(_ sender: Any) {
        let otpCode: String = "\(firstInp.text ?? "")\(secondInp.text ?? "")\(thirdInp.text ?? "")\(fourthInp.text ?? "")\(fifthInp.text ?? "")\(sixInp.text ?? "")"
        let email: String? = UserDefaultHelper.shared.get(key: .userEmail)
        
        self.presenter?.otp(email: String(email ?? ""), otp: otpCode)
        print("=====================================")
        print("> Registered e-mail's user: \(email ?? "")")
    }
    
    var presenter: OTPPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Text Field
        firstInp.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        firstInp.delegate = self
        secondInp.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        secondInp.delegate = self
        thirdInp.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        thirdInp.delegate = self
        fourthInp.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        fourthInp.delegate = self
        fifthInp.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        fifthInp.delegate = self
        sixInp.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        sixInp.delegate = self

        // After 50s doesn't input the OTP back to Login Page
        DispatchQueue.main.asyncAfter(deadline: .now() + 50.0) {
            self.presenter?.backToLogin()
        }
    }
    
    let maxLength: Int = 1
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location > maxLength - 1 {
            firstInp.text?.removeLast()
            secondInp.text?.removeLast()
            thirdInp.text?.removeLast()
            fourthInp.text?.removeLast()
            fifthInp.text?.removeLast()
            sixInp.text?.removeLast()
        }
        return true
        }

}

extension OTPViewController: OTPView {
    func showError() {
        let alert = UIAlertController(
            title: "OTP Message!", message: "OTP is succeed to set!", preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.presenter?.backToLogin()
        }
    }
}
