//
//  SuccessTransferRouter.swift
//  SuccessTransfer
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import UIKit

protocol SuccessTransferRouter {
    func navigateToHome()
}
