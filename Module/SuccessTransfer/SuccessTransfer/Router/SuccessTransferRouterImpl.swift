//
//  SuccessTransferRouterImpl.swift
//  SuccessTransfer
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import UIKit
import Core

public class SuccessTransferRouterImpl {
    
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "id.mandiri.dip.SuccessTransfer")
        let vc = SuccessTransferViewController(nibName: "SuccessTransferViewController", bundle: bundle)
        let router = SuccessTransferRouterImpl()
        
        let presenter = SuccessTransferPresenterImpl(router: router)
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension SuccessTransferRouterImpl: SuccessTransferRouter {
    func navigateToHome() {
        AppRouter.shared.navigateToHome()
    }
}
