//
//  SuccessTransferViewController.swift
//  SuccessTransfer
//
//  Created by Dodsstwn on 30/05/21.
//

import UIKit
import Core
import Kingfisher

class SuccessTransferViewController: UIViewController {
    
    
    var presenter: SuccessTransferPresenter?
    
    @IBOutlet weak var greenChecked: UIImageView!
    @IBOutlet weak var amountTxt: UILabel!
    @IBOutlet weak var balanceLeftTxt: UILabel!
    @IBOutlet weak var timeTxt: UILabel!
    @IBOutlet weak var dateTxt: UILabel!
    @IBOutlet weak var notesTxt: UILabel!
    @IBOutlet weak var receiverUserImage: UIImageView!
    @IBOutlet weak var receiverUserName: UILabel!
    @IBOutlet weak var receiverUserPhone: UILabel!
    
    let image: String? = UserDefaultHelper.shared.get(key: .userImageUrl)
    let name: String? = UserDefaultHelper.shared.get(key: .userName)
    let phone: String? = UserDefaultHelper.shared.get(key: .userPhone)
    let amount: String? = UserDefaultHelper.shared.get(key: .amountTransaction)
    let date: String? = UserDefaultHelper.shared.get(key: .dateTransaction)
    let time: String? = UserDefaultHelper.shared.get(key: .timeTransaction)
    let notes: String? = UserDefaultHelper.shared.get(key: .notesTransaction)
    let balance: String? = UserDefaultHelper.shared.get(key: .userBalance)
    
    @IBAction func backToHome(_ sender: Any) {
        self.presenter?.backToHome()
        
        // Delete value on UserDefaultHelper to refresh transaction
        UserDefaultHelper.shared.remove(key: .amountTransaction)
        UserDefaultHelper.shared.remove(key: .dateTransaction)
        UserDefaultHelper.shared.remove(key: .timeTransaction)
        UserDefaultHelper.shared.remove(key: .notesTransaction)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let amountInIdr: Int = Int(amount ?? "")!
        amountTxt.text = amountInIdr.formatToIdr()
        balanceLeftTxt.text = balance ?? ""
        timeTxt.text = time ?? ""
        dateTxt.text = date ?? ""
        notesTxt.text = notes ?? ""
        receiverUserName.text = name ?? ""
        receiverUserPhone.text = phone ?? ""
        greenChecked.image = UIImage(named: "success", in: Bundle(identifier: "com.mandiri.SuccessTransfer"), compatibleWith: nil)
        
        let url = URL(string: image ?? "")
        receiverUserImage.kf.setImage(with: url)
    }
}
