//
//  SuccessTransferPresenterImpl.swift
//  SuccessTransfer
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import Core

class SuccessTransferPresenterImpl: SuccessTransferPresenter {
    
    let router: SuccessTransferRouter
    
    init(router: SuccessTransferRouter) {
        self.router = router
    }
    
    func backToHome() {
        self.router.navigateToHome()
    }
}
