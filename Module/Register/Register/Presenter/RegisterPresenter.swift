//
//  RegisterPresenter.swift
//  Register
//
//  Created by Dodsstwn on 26/05/21.
//

import Foundation

protocol RegisterPresenter {
    func register(email: String, username: String, password: String)
    func showLogin()
    func goToOTP()
}
