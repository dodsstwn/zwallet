//
//  RegisterPresenterImpl.swift
//  Register
//
//  Created by Dodsstwn on 26/05/21.
//

import Foundation

class RegisterPresenterImpl: RegisterPresenter {
    
    let view: RegisterView
    let interactor: RegisterInteractor
    let router: RegisterRouter
    
    init(view: RegisterView, interactor: RegisterInteractor, router: RegisterRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func register(email: String, username: String, password: String) {
        self.interactor.postRegisterData(email: email, username: username, password: password)
    }
    
    func showLogin() {
        self.router.navigateToLogin()
    }
    
    func goToOTP() {
        self.router.navigateToOTP()
    }
}

extension RegisterPresenterImpl: RegisterInteractorOutput {
    func authenticationResult(isSuccess: Bool) {
        if isSuccess {
            self.router.navigateToLogin()
        } else {
            self.view.showError()
        }
    }
}
