//
//  RegisterView.swift
//  Register
//
//  Created by Dodsstwn on 26/05/21.
//

import Foundation

protocol RegisterView {
    func showError()
}
