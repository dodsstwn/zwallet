//
//  RegisterViewController.swift
//  Register
//
//  Created by Dodsstwn on 25/05/21.
//

import UIKit
import Core

class RegisterViewController: UIViewController {

    
    @IBOutlet weak var textUsername: UITextField!
    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    
    var presenter: RegisterPresenter?
    
    @IBAction func signUpButton(_ sender: Any) {
        let username: String = textUsername.text ?? ""
        let email: String = textEmail.text ?? ""
        let password: String = textPassword.text ?? ""
    
        self.presenter?.register(email: email, username: username, password: password)
        UserDefaultHelper.shared.set(key: .userEmail, value: email)
        let emailTemp: String? = UserDefaultHelper.shared.get(key: .userEmail)
        
        print("> Register Data in Temporary Array")
        print("===================================")
        print("- E-mail: \(emailTemp ?? "")")
    }
    
    @IBAction func loginButton(_ sender: Any) {
        presenter?.showLogin()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

extension RegisterViewController: RegisterView {
    func showError() {
        let alert = UIAlertController(
            title: "Register Message!", message: "Your register process has been success, please check your email for the OTP!", preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.presenter?.goToOTP()
        }
    }
}
