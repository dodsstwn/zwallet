//
//  RegisterInteractorOutput.swift
//  Register
//
//  Created by Dodsstwn on 26/05/21.
//

import Foundation

protocol RegisterInteractorOutput {
    func authenticationResult(isSuccess: Bool)
}
