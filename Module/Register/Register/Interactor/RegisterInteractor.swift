//
//  RegisterInteractor.swift
//  Register
//
//  Created by Dodsstwn on 26/05/21.
//

import Foundation

protocol RegisterInteractor {
    func postRegisterData(email: String, username: String, password: String)
}
