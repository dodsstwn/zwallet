//
//  RegisterInteractorImpl.swift
//  Register
//
//  Created by Dodsstwn on 26/05/21.
//

import Foundation
import Core

class RegisterInteractorImpl: RegisterInteractor {
    
    var interactorOutput: RegisterInteractorOutput?
    let authNetworkManager: AuthNetworkManager
    
    
    init(networkManager: AuthNetworkManager) {
        self.authNetworkManager = networkManager
    }
    
    func postRegisterData(email: String, username: String, password: String) {
        // hit api untuk register
        self.authNetworkManager.register(email: email, username: username, password: password) { data, error in
            if let registerData = data {
                // menyimpan user token ke userDefault
                UserDefaultHelper.shared.set(key: .userToken, value: registerData.token)
                // menyimpan user email ke userDefault
                UserDefaultHelper.shared.set(key: .userEmail, value: registerData.email)
                
                // memberitahu ke presenter jika berhasil
                self.interactorOutput?.authenticationResult(isSuccess: true)
            } else {
                // mermberitahu ke presenter jika gagal
                self.interactorOutput?.authenticationResult(isSuccess: false)
            }
        }
    }
}
