//
//  RegisterRouter.swift
//  Register
//
//  Created by Dodsstwn on 26/05/21.
//

import Foundation

protocol RegisterRouter {
    func navigateToHome()
    func navigateToLogin()
    func navigateToOTP()
}
