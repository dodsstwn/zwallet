//
//  RegisterRouterImpl.swift
//  Register
//
//  Created by Dodsstwn on 26/05/21.
//

import Foundation
import UIKit
import Core

public class RegisterRouterImpl {
    
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "id.mandiri.dip.Register")
        let vc = RegisterViewController(nibName: "RegisterViewController", bundle: bundle)
        let networkManager = AuthNetworkManagerImpl()
        let router = RegisterRouterImpl()
        let interactor = RegisterInteractorImpl(networkManager: networkManager)
        let presenter = RegisterPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension RegisterRouterImpl: RegisterRouter {
    func navigateToHome() {
        NotificationCenter.default.post(name: Notification.Name("reloadRootView"), object: nil)
    }
    
    func navigateToLogin() {
        // Notification center
        AppRouter.shared.navigateLogin()
        print("< from navigateToLogin!")
    }
    
    func navigateToOTP() {
        AppRouter.shared.navigateToOTP()
    }
}
