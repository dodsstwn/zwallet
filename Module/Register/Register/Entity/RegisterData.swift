//
//  RegisterData.swift
//  Register
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation

struct RegisterData {
    var id: String?
    var username: String?
    var email: String?
    var password: String?
}

var account: [RegisterData] = []
