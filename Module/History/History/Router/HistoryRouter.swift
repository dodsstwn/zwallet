//
//  HistoryRouter.swift
//  History
//
//  Created by Dodsstwn on 25/05/21.
//

import Foundation
import UIKit

protocol HistoryRouter {
    func navigateToHome()
    func navigateToLogin()
}
