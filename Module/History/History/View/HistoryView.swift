//
//  HistoryView.swift
//  History
//
//  Created by Dodsstwn on 25/05/21.
//

import Foundation
import Core

protocol HistoryView {
    func showTransactionData(transactions: [TransactionEntity])
}
