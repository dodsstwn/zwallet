//
//  HistoryDataSource.swift
//  History
//
//  Created by Dodsstwn on 25/05/21.
//

import Foundation
import UIKit
import Core

class HistoryDataSource: NSObject, UITableViewDataSource {
    
    var viewController: HistoryViewController!
    var transactions: [TransactionEntity] = []
    var transactionsMonth: [TransactionEntity] = []
    let typeShowData: Array = ["This Week", "This Month"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return transactions.count
        } else {
            if transactions.count == 0 {
                return 1
            } else {
                return transactions.count
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if transactions.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyHandlingCell", for: indexPath) as! EmptyHandlingCell
                cell.titleEmptyCell.text = "Empty data for The History"
                cell.descEmptyCell.text = "Please create your new Transaction to fill the History"
                self.viewController.tableView.separatorColor = .clear
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell", for: indexPath) as! TransactionCell
                cell.showData(transaction: self.transactions[indexPath.row])
                return cell
            }
        } else {
            if transactions.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyHandlingCell", for: indexPath) as! EmptyHandlingCell
                cell.titleEmptyCell.text = "Empty data for The History"
                cell.descEmptyCell.text = "Please create your new Transaction to fill the History"
                self.viewController.tableView.separatorColor = .clear
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell", for: indexPath) as! TransactionCell
                cell.showData(transaction: self.transactionsMonth[indexPath.row])
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return typeShowData[section]
    }
}
