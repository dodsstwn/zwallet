//
//  HistoryViewController.swift
//  History
//
//  Created by Dodsstwn on 25/05/21.
//

import UIKit
import Core

class HistoryViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBAction func backButton(_ sender: Any) {
        presenter?.showHome()
    }
    
    var dataSource = HistoryDataSource()
    var presenter: HistoryPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        
        self.presenter?.loadTransaction()
    }
    
    func setupTableView() {
        self.dataSource.viewController = self
        self.tableView.register(UINib(nibName: "TransactionCell", bundle: Bundle(identifier: "id.mandiri.dip.Core")), forCellReuseIdentifier: "TransactionCell")
        self.tableView.register(UINib(nibName: "EmptyHandlingCell", bundle: Bundle(identifier: "id.mandiri.dip.Core")), forCellReuseIdentifier: "EmptyHandlingCell")
        self.tableView.dataSource = self.dataSource
        self.tableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 100
        default:
            return 100
        }
    }
}

extension HistoryViewController: HistoryView {
    func showTransactionData(transactions: [TransactionEntity]) {
        self.dataSource.transactions = transactions
        self.dataSource.transactionsMonth = transactions
        self.tableView.reloadData()
    }
}
