//
//  HistoryInteractorImpl.swift
//  History
//
//  Created by Dodsstwn on 25/05/21.
//

import Foundation
import Core

class HistoryInteractorImpl: HistoryInteractor {
    
    var interactorOutput: HistoryInteractorOutput?
    
    let balanceNetworkManager: BalanceNetworkManager
    let invoiceNetworkManager: InvoiceNetworkManager
    
    init(balanceNetworkManager: BalanceNetworkManager, invoiceNetworkManager: InvoiceNetworkManager) {
        self.balanceNetworkManager = balanceNetworkManager
        self.invoiceNetworkManager = invoiceNetworkManager
    }
    
    func getTransaction() {
        self.invoiceNetworkManager.getAllInvoice { (data, error) in
            var transactions: [TransactionEntity] = []
            
            data?.forEach({ (invoiceData) in
                transactions.append(TransactionEntity(name: invoiceData.name, type: invoiceData.type, imageUrl: "\(AppConstant.baseUrl)\(invoiceData.image)", amount: invoiceData.amount, notes: invoiceData.notes))
                
                self.interactorOutput?.loadedTransaction(transactions: transactions)
            })
        }
    }
}
