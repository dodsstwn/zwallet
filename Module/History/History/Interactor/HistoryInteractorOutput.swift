//
//  HistoryInteractorOutput.swift
//  History
//
//  Created by Dodsstwn on 25/05/21.
//

import Foundation
import Core

protocol HistoryInteractorOutput {
    func loadedTransaction(transactions: [TransactionEntity])
}
