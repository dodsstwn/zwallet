//
//  HistoryInteractor.swift
//  History
//
//  Created by Dodsstwn on 25/05/21.
//

import Foundation

protocol HistoryInteractor {
    func getTransaction()
}
