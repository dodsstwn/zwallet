//
//  HomePresenterImpl.swift
//  History
//
//  Created by Dodsstwn on 25/05/21.
//

import Foundation
import Core

class HistoryPresenterImpl: HistoryPresenter {
    
    let view: HistoryView
    let interactor: HistoryInteractor
    let router: HistoryRouter
    
    init(view: HistoryView, interactor: HistoryInteractor, router: HistoryRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadTransaction() {
        self.interactor.getTransaction()
    }
    
    func showHome() {
        self.router.navigateToHome()
    }
}

extension HistoryPresenterImpl: HistoryInteractorOutput {
    func loadedTransaction(transactions: [TransactionEntity]) {
        self.view.showTransactionData(transactions: transactions)
    }
}
