//
//  HomeRouter.swift
//  Home
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation
import UIKit

protocol HomeRouter {
    func navigateToHistory(viewController: UIViewController)
    func navigateToLogin()
    func navigateToProfile()
}
