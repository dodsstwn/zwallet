//
//  HomeViewController.swift
//  Home
//
//  Created by Dodsstwn on 24/05/21.
//

import UIKit
import Core

class HomeViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource = HomeDataSource()
    
    var presenter: HomePresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        
        self.presenter?.loadProfile()
        self.presenter?.loadTransaction()
    }
    
    func setupTableView() {
        self.dataSource.viewController = self
        self.tableView.register(UINib(nibName: "DashboardCell", bundle: Bundle(identifier: "id.mandiri.dip.Home")), forCellReuseIdentifier: "DashboardCell")
        self.tableView.register(UINib(nibName: "TransactionCell", bundle: Bundle(identifier: "id.mandiri.dip.Core")), forCellReuseIdentifier: "TransactionCell")
        self.tableView.register(UINib(nibName: "EmptyHandlingCell", bundle: Bundle(identifier: "id.mandiri.dip.Core")), forCellReuseIdentifier: "EmptyHandlingCell")
        
        self.tableView.dataSource = self.dataSource
        self.tableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 337
        default:
            return 100
        }
    }
}

extension HomeViewController: DashboardCellDelegate {
    
    func showAllTransaction() {
        self.presenter?.showHistory(viewController: self)
    }
    
    func logout() {
        self.presenter?.logout()
    }
    
    func showProfile() {
        self.presenter?.showProfile()
    }
    
}

extension HomeViewController: HomeView {
    
    func showUserProfileData(userProfile: NewUserEntity) {
        self.dataSource.userProfile = userProfile
        self.tableView.reloadData()
    }
    
    func showTransactionData(transactions: [TransactionEntity]) {
        self.dataSource.transactions = transactions
        self.tableView.reloadData()
    }
}
