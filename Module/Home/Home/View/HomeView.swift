//
//  HomeView.swift
//  Home
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation
import Core

protocol HomeView {
    func showUserProfileData(userProfile: NewUserEntity)
    func showTransactionData(transactions: [TransactionEntity])
}
