//
//  DashboardCellDelegate.swift
//  Home
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation

protocol DashboardCellDelegate {
    func showAllTransaction()
    func logout()
    func showProfile()
}
