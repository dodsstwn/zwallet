//
//  DashboardCell.swift
//  Home
//
//  Created by Dodsstwn on 24/05/21.
//

import UIKit
import Kingfisher
import Core

class DashboardCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBAction func profileButton(_ sender: Any) {
        self.delegate?.showProfile()
        print("< Go To Profile!")
    }
    
    @IBAction func logoutButton(_ sender: Any) {
        self.delegate?.logout()
    }
    
    
    @IBAction func transferButton(_ sender: Any) {
        AppRouter.shared.navigateToContact()
    }
    
    var delegate: DashboardCellDelegate?
    var presenter: HomePresenter?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func showData(userProfile: NewUserEntity) {
        self.nameLabel.text = userProfile.name
        self.balanceLabel.text = userProfile.balance.formatToIdr()
        self.phoneNumberLabel.text = userProfile.phone
        
        let url = URL(string: userProfile.imageUrl)
        self.profileImage.kf.setImage(with: url)
    }
    
    
    @IBAction func showAllTransaction(_ sender: Any) {
        self.delegate?.showAllTransaction()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
