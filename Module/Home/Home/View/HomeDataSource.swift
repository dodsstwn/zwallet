//
//  HomeDataSource.swift
//  Home
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation
import UIKit
import Core

class HomeDataSource: NSObject, UITableViewDataSource {
    
    var viewController: HomeViewController!
    var userProfile: NewUserEntity = NewUserEntity(name: "", phone: "", balance: 0, imageUrl: "")
    var transactions: [TransactionEntity] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            if transactions.count == 0 {
                return 1
            } else {
                return transactions.count
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardCell", for: indexPath) as! DashboardCell
            cell.showData(userProfile: self.userProfile)
            cell.delegate = self.viewController
            return cell
        } else {
            if transactions.count == 0 {
                // Fill with the new cell
                let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyHandlingCell", for: indexPath) as! EmptyHandlingCell
                cell.titleEmptyCell.text = "Empty data for The Transactions"
                cell.descEmptyCell.text = "Please create your new Transaction"
                self.viewController.tableView.separatorColor = .clear
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell", for: indexPath) as! TransactionCell
                cell.showData(transaction: self.transactions[indexPath.row])
                self.viewController.tableView.separatorColor = .gray
                return cell
            }
        }
    }
}
