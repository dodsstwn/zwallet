//
//  HomeInteractorOutput.swift
//  Home
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation
import Core

protocol HomeInteractorOutput {
    func loadedUserProfileData(userProfile: NewUserEntity)
    func loadedTransaction(transactions: [TransactionEntity])
}
