//
//  HomeInteractor.swift
//  Home
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation

protocol HomeInteractor {
    func getUserProfile()
    func getTransaction()
}
