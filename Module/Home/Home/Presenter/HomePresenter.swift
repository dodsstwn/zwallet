//
//  HomePresenter.swift
//  Home
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation
import UIKit

protocol HomePresenter {
    func loadProfile()
    func showProfile()
    func loadTransaction()
    func showHistory(viewController: UIViewController)
    func logout()
}
