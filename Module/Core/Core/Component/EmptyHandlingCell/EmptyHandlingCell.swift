//
//  EmptyHandlingCell.swift
//  Core
//
//  Created by Dodsstwn on 31/05/21.
//

import UIKit

public class EmptyHandlingCell: UITableViewCell {

    @IBOutlet weak public var titleEmptyCell: UILabel!
    @IBOutlet weak public var descEmptyCell: UILabel!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    public override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
