//
//  AppRouter.swift
//  Core
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation
import UIKit

public class AppRouter {
    public static let shared: AppRouter = AppRouter()
    
    public var loginScene: (() -> ())? = nil
    public func navigateLogin() {
        self.loginScene?()
    }
    
    public var homeScene: (() -> ())? = nil
    public func navigateToHome() {
        self.homeScene?()
    }
    
    public var historyScene: (() -> ())? = nil
    public func navigateToHistory() {
        self.historyScene?()
    }
    
    public var registerScene: (() -> ())? = nil
    public func navigateToRegister() {
        self.registerScene?()
    }
    
    public var otpScene: (() -> ())? = nil
    public func navigateToOTP() {
        self.otpScene?()
    }
    
    public var pinScene: (() -> ())? = nil
    public func navigateToPIN() {
        self.pinScene?()
    }
    
    public var profileScene: (() -> ())? = nil
    public func navigateToProfile() {
        self.profileScene?()
    }
    
    public var contactScene: (() -> ())? = nil
    public func navigateToContact() {
        self.contactScene?()
    }
    
    public var transferScene: (() -> ())? = nil
    public func navigateToTransfer() {
        self.transferScene?()
    }
    
    public var confirmTransferScene: (() -> ())? = nil
    public func navigateToConfirmTransfer() {
        self.confirmTransferScene?()
    }
    
    public var transferPINScene: (() -> ())? = nil
    public func navigateToTransferPIN() {
        self.transferPINScene?()
    }
    
    public var successTransferScene: (() -> ())? = nil
    public func navigateToSuccessTransfer() {
        self.successTransferScene?()
    }
    
}
