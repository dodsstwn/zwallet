//
//  NewUserEntity.swift
//  Core
//
//  Created by Dodsstwn on 29/05/21.
//

import Foundation

public struct NewUserEntity {
    public var name: String
    public var phone: String
    public var balance: Int
    public var imageUrl: String
    public init (name: String, phone: String, balance: Int, imageUrl: String){
        self.name = name
        self.phone = phone
        self.balance = balance
        self.imageUrl = imageUrl
    }
}

public var user: [NewUserEntity] = []
