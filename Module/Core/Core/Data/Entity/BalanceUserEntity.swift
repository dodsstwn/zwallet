//
//  BalanceUserEntity.swift
//  Core
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation

public struct BalanceUserEntity {
    
    public var balance: Int
    
    public init (balance: Int) {
        self.balance = balance
    }
}

public var checkBalance: [BalanceUserEntity] = []
