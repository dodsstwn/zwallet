//
//  ProfileApi.swift
//  Core
//
//  Created by Dodsstwn on 28/05/21.
//

import Foundation
import Moya

public enum ProfileApi {
    case getProfile
    case getContact
}

extension ProfileApi: TargetType {
    public var baseURL: URL {
        return URL(string: AppConstant.baseUrl)!
    }
    
    public var method: Moya.Method {
        return .get
    }
    
    public var task: Task {
        switch self {
        case .getProfile:
            return .requestPlain
        case .getContact:
            return .requestPlain
        }
    }
    
    public var headers: [String : String]? {
        let token: String = UserDefaultHelper.shared.get(key: .userToken) ?? ""
        return [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(token)"
        ]
    }
    
    public var path: String {
        switch self {
        case .getProfile:
            return "/user/myProfile"
        case .getContact:
            return "/tranfer/contactUser"
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
}
