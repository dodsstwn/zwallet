//
//  TransferNetworkManager.swift
//  Core
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation

public protocol TransferNetworkManager {
    func createNewTransaction(receiver: Int, amount: Int, notes: String, completion: @escaping (TransferResponse?, Error?) -> ())
}
