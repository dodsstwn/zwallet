//
//  ProfileNetworkManager.swift
//  Core
//
//  Created by Dodsstwn on 28/05/21.
//

import Foundation

public protocol ProfileNetworkManager {
    func getProfile(completion: @escaping ([GetProfileDataResponse]?, Error?) -> ())
    func getContact(completion: @escaping ([GetContactDataResponse]?, Error?) -> ())
}
