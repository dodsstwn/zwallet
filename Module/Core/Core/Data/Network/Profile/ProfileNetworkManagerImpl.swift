//
//  ProfileNetworkManagerImpl.swift
//  Core
//
//  Created by Dodsstwn on 28/05/21.
//

import Foundation
import Moya

public class ProfileNetworkManagerImpl: ProfileNetworkManager {
    
    public init() {
    }
    
    public func getProfile(completion: @escaping ([GetProfileDataResponse]?, Error?) -> ()) {
        
        let provider = MoyaProvider<ProfileApi>(plugins: [NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])
        
        provider.request(.getProfile) { response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let getProfileResponse = try decoder.decode(GetProfileResponse.self, from: result.data)
                    print("++ \(getProfileResponse.data)")
                    completion(getProfileResponse.data, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    public func getContact(completion: @escaping ([GetContactDataResponse]?, Error?) -> ()) {
        let provider = MoyaProvider<ProfileApi>(plugins: [NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])
        provider.request(.getContact) { response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let getContactResponse = try decoder.decode(GetContactResponse.self, from: result.data)
                    print("++ \(getContactResponse.data)")
                    completion(getContactResponse.data, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
