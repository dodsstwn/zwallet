//
//  GetContactResponse.swift
//  Core
//
//  Created by Dodsstwn on 29/05/21.
//

import Foundation

public struct GetContactResponse: Codable {
    var status: Int
    
    var data: [GetContactDataResponse]
}
