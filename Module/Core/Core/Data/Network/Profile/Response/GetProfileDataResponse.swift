//
//  GetProfileDataResponse.swift
//  Core
//
//  Created by Dodsstwn on 28/05/21.
//

import Foundation

public struct GetProfileDataResponse: Codable {
    public var id: Int
    public var firstname: String
    public var lastname: String
    public var email: String
    public var phone: String
    public var image: String
}
