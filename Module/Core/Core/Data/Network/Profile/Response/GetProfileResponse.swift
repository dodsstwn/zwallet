//
//  GetProfileResponse.swift
//  Core
//
//  Created by Dodsstwn on 28/05/21.
//

import Foundation

public struct GetProfileResponse: Codable {
    var status: Int
    var message: String
    var data: [GetProfileDataResponse]
}
