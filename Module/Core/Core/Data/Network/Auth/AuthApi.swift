//
//  AuthApi.swift
//  Core
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation
import Moya

public enum AuthApi {
    case login(email: String, password: String)
    case register(username: String, email: String, password: String)
    case otp(email: String, otp: String)
    case pin(pin: String)
}

extension AuthApi: TargetType {
    public var path: String {
        switch self {
        case .login:
            return "/auth/login"
        case .register:
            return "/auth/signup"
        case .otp(let email, let otp):
            print("/auth/activate/\(email)/\(otp)")
            return "/auth/activate/\(email)/\(otp)"
        case .pin:
            return "/auth/PIN"
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .login(let email, let password):
            return .requestParameters(
                parameters: ["email": email, "password": password],
                encoding: JSONEncoding.default)
        case .register(username: let username, email: let email, password: let password):
            return .requestParameters(
                parameters: ["email": email, "username": username, "password": password],
                encoding: JSONEncoding.default)
        case .otp:
            // The GET method doesn't send any body
            return .requestPlain
        case .pin(pin: let pin):
            // The PATCH method
            return .requestParameters(parameters: ["PIN": pin], encoding: JSONEncoding.default)
        }
        
    }
    
    public var baseURL: URL {
        return URL(string: AppConstant.baseUrl)!
    }
    
    public var method: Moya.Method {
        switch self {
        case .login:
            return .post
        case .register:
            return .post
        case .otp:
            return .get
        case .pin:
            return .patch
        }
    }
    
    public var headers: [String : String]? {
        switch self {
        case .pin:
            let token: String = UserDefaultHelper.shared.get(key: .userToken) ?? ""
            return [
                "Content-Type": "application/json",
                "Authorization" : "Bearer \(token)"
            ]
        case .otp:
            let token: String = UserDefaultHelper.shared.get(key: .userToken) ?? ""
            return [
                "Content-Type": "application/json",
                "Authorization" : "Bearer \(token)"
            ]
        default:
            return [
                "Content-Type": "application/json"
            ]
        }
    }
}
