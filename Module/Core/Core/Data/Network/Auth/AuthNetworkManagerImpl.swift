//
//  AuthNetworkManagerImpl.swift
//  Core
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation
import Moya

public class AuthNetworkManagerImpl: AuthNetworkManager {
    
    public init() {
        
    }
    
    public func login(email: String, password: String, completion: @escaping (LoginResponse?, Error?) -> ()) {
        let provider = MoyaProvider<AuthApi>(plugins: [NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])
        provider.request(.login(email: email, password: password)) { response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let loginResponse = try decoder.decode(LoginResponse.self, from: result.data)
                    completion(loginResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    public func register(email: String, username: String, password: String, completion: @escaping (RegisterDataResponse?, Error?) -> ()) {
        let provider = MoyaProvider<AuthApi>(plugins: [NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])
        provider.request(.register(username: username, email: email, password: password)) { response in switch response {
        case .success(let result):
            let decoder = JSONDecoder()
            do {
                let registerResponse = try decoder.decode(RegisterResponse.self, from: result.data)
                completion(registerResponse.data, nil)
            } catch let error {
                completion(nil, error)
            }
        case .failure(let error):
            completion(nil, error)
        }}
    }
    
    public func otp(email: String, otp: String, completion: @escaping (OTPDataResponse?, Error?) -> ()) {
        let provider = MoyaProvider<AuthApi>(plugins: [NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])
        provider.request(.otp(email: email, otp: otp)) {
            response in switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let otpResponse = try decoder.decode(OTPResponse.self, from: result.data)
                    completion(otpResponse.data, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    public func pin(pin: String, completion: @escaping (PINResponse?, Error?) -> ()) {
        let provider = MoyaProvider<AuthApi>(plugins: [NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])
        provider.request(.pin(pin: pin)) {
            response in switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let pinResponse = try decoder.decode(PINResponse.self, from: result.data)
                    completion(pinResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
}
