//
//  PINDataResponse.swift
//  Core
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation

public struct PINDataResponse: Codable {
    public var id: Int
    public var email: String
    public var pin: String
    public var token: String
}
