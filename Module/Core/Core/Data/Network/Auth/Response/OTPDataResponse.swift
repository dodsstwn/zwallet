//
//  OTPDataResponse.swift
//  Core
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation

public struct OTPDataResponse: Codable {
    public var id: Int
    public var email: String
    public var otp: String
    public var token: String
}
