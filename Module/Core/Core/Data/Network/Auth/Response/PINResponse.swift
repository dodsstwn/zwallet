//
//  PINResponse.swift
//  Core
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation

public struct PINResponse: Codable {
    public var status: Int
    public var message: String
    public var data: PINDataResponse
}
