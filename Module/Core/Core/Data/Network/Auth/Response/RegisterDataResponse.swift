//
//  RegisterDataResponse.swift
//  Core
//
//  Created by Dodsstwn on 26/05/21.
//

import Foundation

public struct RegisterDataResponse: Codable {
    public var id: Int
    public var username: String
    public var password: String
    public var email: String
    public var token: String
}
