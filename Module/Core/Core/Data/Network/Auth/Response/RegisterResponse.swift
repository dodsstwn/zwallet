//
//  RegisterResponse.swift
//  Core
//
//  Created by Dodsstwn on 26/05/21.
//

import Foundation

public struct RegisterResponse: Codable {
    public var status: Int
    public var message: String
    public var data: RegisterDataResponse
}
