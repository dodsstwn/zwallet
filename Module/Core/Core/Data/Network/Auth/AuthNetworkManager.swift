//
//  AuthNetworkManager.swift
//  Core
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation

public protocol AuthNetworkManager {
    func login(email: String, password: String, completion: @escaping (LoginResponse?, Error?) -> ())
    func register(email: String, username: String, password: String, completion: @escaping (RegisterDataResponse?, Error?) -> ())
    func otp(email: String, otp: String, completion: @escaping
        (OTPDataResponse?, Error?) -> ())
    func pin(pin: String, completion: @escaping (PINResponse?, Error?) -> ())
}
