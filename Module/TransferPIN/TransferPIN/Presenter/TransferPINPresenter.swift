//
//  TransferPINPresenter.swift
//  TransferPIN
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import UIKit

protocol TransferPINPresenter {
    func sendTransferData()
    func hitAPITransfer(receiver: Int, amount: Int, notes: String)
    func backToConfirmTransfer()
}
