//
//  TransferPINPresenterImpl.swift
//  TransferPIN
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import UIKit
import Core

class TransferPINPresenterImpl: TransferPINPresenter {
    
    let view: TransferPINView
    let interactor: TransferPINInteractor
    let router: TransferPINRouter
    
    init(view: TransferPINView, interactor: TransferPINInteractor, router: TransferPINRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    
    func sendTransferData() {
        self.router.navigateToDataPINCheck()
    }
    
    func hitAPITransfer(receiver: Int, amount: Int, notes: String) {
        self.interactor.postTransaction(receiver: receiver, amount: amount, notes: notes)
    }
    
    func backToConfirmTransfer() {
        self.router.navigateToConfirmTransfer()
    }
}

extension TransferPINPresenterImpl: TransferPINInteractorOutput {
    func authenticationResult(isSuccess: Bool, status: Int) {
        if status == 200 {
            self.view.showError(passing: isSuccess, status: status)
        } else {
            self.view.showError(passing: isSuccess, status: status)
        }
    }
}
