//
//  TransferPIN.swift
//  TransferPIN
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation

protocol TransferPINView {
    func showError(passing: Bool, status: Int)
    func showWrongPIN()
}
