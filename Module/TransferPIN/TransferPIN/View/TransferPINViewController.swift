//
//  TransferPINViewController.swift
//  TransferPIN
//
//  Created by Dodsstwn on 30/05/21.
//

import UIKit
import Core

class TransferPINViewController: UIViewController, UITextFieldDelegate {

    var presenter: TransferPINPresenter?
    let amount: String? = UserDefaultHelper.shared.get(key: .amountTransaction)
    let notes: String? = UserDefaultHelper.shared.get(key: .notesTransaction)
    let id: Int? = UserDefaultHelper.shared.get(key: .userID)
    var statusTransfer: Int = 0
    var selectPassing: Bool = false
    
    @IBAction func backButton(_ sender: Any) {
        AppRouter.shared.navigateToConfirmTransfer()
    }
    
    @IBOutlet weak var firstTxt: UITextField!
    @IBOutlet weak var secondTxt: UITextField!
    @IBOutlet weak var thirdTxt: UITextField!
    @IBOutlet weak var fourthTxt: UITextField!
    @IBOutlet weak var fifthTxt: UITextField!
    @IBOutlet weak var sixTxt: UITextField!
    
    
    @IBAction func transferButton(_ sender: Any) {
    
        let tempPIN: String = "\(firstTxt.text ?? "")\(secondTxt.text ?? "")\(thirdTxt.text ?? "")\(fourthTxt.text ?? "")\(fifthTxt.text ?? "")\(sixTxt.text ?? "")"
        UserDefaultHelper.shared.set(key: .userPin, value: tempPIN)
        self.presenter?.hitAPITransfer(receiver: id ?? 0, amount: Int(amount ?? "")!, notes: notes ?? "")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            if self.statusTransfer == 200 {
                // If success transfer
                self.presenter?.sendTransferData()
            } else {
                // If the PIN doesn't match
                self.showWrongPIN()
            }
        }
        
        // print amount
        print(">> \(amount ?? "")")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Text Field
        firstTxt.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        firstTxt.delegate = self
        secondTxt.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        secondTxt.delegate = self
        thirdTxt.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        thirdTxt.delegate = self
        fourthTxt.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        fourthTxt.delegate = self
        fifthTxt.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        fifthTxt.delegate = self
        sixTxt.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        sixTxt.delegate = self
    }
    
    let maxLength: Int = 1
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if range.location > maxLength - 1 {
            firstTxt.text?.removeLast()
            secondTxt.text?.removeLast()
            thirdTxt.text?.removeLast()
            fourthTxt.text?.removeLast()
            fifthTxt.text?.removeLast()
            sixTxt.text?.removeLast()
        }
        return true
        }
}

extension TransferPINViewController: TransferPINView {
    func showError(passing: Bool, status: Int) {
        if passing {
            selectPassing = true
            statusTransfer = status
        } else {
            selectPassing = false
            statusTransfer = status
        }
    }
    
    func showWrongPIN() {
        // If the PIN doesn't match
        let alert = UIAlertController(title: "Alert Notification!", message: "Your PIN is not match!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
