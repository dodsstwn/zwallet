//
//  TransferPINRouter.swift
//  TransferPIN
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import UIKit


protocol TransferPINRouter {
    func navigateToDataPINCheck()
    func navigateToConfirmTransfer()
}
