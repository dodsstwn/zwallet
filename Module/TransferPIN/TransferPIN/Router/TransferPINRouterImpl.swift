//
//  TransferPINRouterImpl.swift
//  TransferPIN
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import UIKit
import Core

public class TransferPINRouterImpl {
    
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "id.mandiri.dip.TransferPIN")
        let vc = TransferPINViewController(nibName: "TransferPINViewController", bundle: bundle)
        let transferNetworkManager = TransferNetworkManagerImpl()
        let router = TransferPINRouterImpl()
        let interactor = TransferPINInteractorImpl(transferNetworkManager: transferNetworkManager)
        let presenter = TransferPINPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension TransferPINRouterImpl: TransferPINRouter {
    func navigateToDataPINCheck() {
        // Move to the final page!
        // AppRouter.shared.navigateToFinalPage()
        AppRouter.shared.navigateToSuccessTransfer()
        print(">> PIN Check")
    }
    
    func navigateToConfirmTransfer() {
        AppRouter.shared.navigateToConfirmTransfer()
    }
}
