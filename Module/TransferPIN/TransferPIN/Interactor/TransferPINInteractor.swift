//
//  TransferPINInteractor.swift
//  TransferPIN
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation

protocol TransferPINInteractor {
    func postTransaction(receiver: Int, amount: Int, notes: String)
}
