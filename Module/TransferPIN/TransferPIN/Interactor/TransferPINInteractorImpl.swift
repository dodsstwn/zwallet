//
//  TransferPINInteractorImpl.swift
//  TransferPIN
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import Core

class TransferPINInteractorImpl: TransferPINInteractor {

    var interactorOutput: TransferPINInteractorOutput?
    var transferNetworkManager: TransferNetworkManager
    
    init(transferNetworkManager: TransferNetworkManager) {
        self.transferNetworkManager = transferNetworkManager
    }
    
    func postTransaction(receiver: Int, amount: Int, notes: String) {
        self.transferNetworkManager.createNewTransaction(receiver: receiver, amount: amount, notes: notes) { data, error in
            
            if let transferData = data {
                self.interactorOutput?.authenticationResult(isSuccess: true, status: data?.status ?? 0)
            } else {
                self.interactorOutput?.authenticationResult(isSuccess: false, status: data?.status ?? 0)
            }
        }
    }
}
