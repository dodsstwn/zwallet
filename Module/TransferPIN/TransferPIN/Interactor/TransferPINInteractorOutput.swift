//
//  TransferPINInteractorOutput.swift
//  TransferPIN
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation

protocol TransferPINInteractorOutput {
    func authenticationResult(isSuccess: Bool, status: Int)
}
