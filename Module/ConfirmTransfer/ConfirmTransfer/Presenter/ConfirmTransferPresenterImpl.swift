//
//  ConfirmTransferPresenterImpl.swift
//  ConfirmTransfer
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import Core

class ConfirmTransferPresenterImpl: ConfirmTransferPresenter {
    
    let view: ConfirmTransferView
    let interactor: ConfirmTransferInteractor
    let router: ConfirmTransferRouter
    
    init(view: ConfirmTransferView, interactor: ConfirmTransferInteractor, router: ConfirmTransferRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadContactData() {
        
    }
    
    func loadBalanceData() {
        self.interactor.getBalance()
    }
    
    func backToTransfer() {
        self.router.navigateToTransfer()
    }
    
    func goToTransferPIN() {
        self.router.navigateToPIN()
    }
}

extension ConfirmTransferPresenterImpl: ConfirmTransferInteractorOutput {
    func loadedData(contact: NewUserEntity) {
        self.view.showDataContact(contact: contact)
    }
    
    func loadedBalance(balance: BalanceUserEntity) {
        self.view.showDataBalance(balance: balance)
    }
}
