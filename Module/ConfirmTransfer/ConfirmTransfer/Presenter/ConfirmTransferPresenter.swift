//
//  ConfirmTransferPresenter.swift
//  ConfirmTransfer
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import UIKit

protocol ConfirmTransferPresenter {
    func loadContactData()
    func loadBalanceData()
    func backToTransfer()
    func goToTransferPIN()
}

