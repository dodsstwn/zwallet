//
//  ConfirmTransferInteractorOutput.swift
//  ConfirmTransfer
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import Core

protocol ConfirmTransferInteractorOutput {
    func loadedData(contact: NewUserEntity)
    func loadedBalance(balance: BalanceUserEntity)
}
