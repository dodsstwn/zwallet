//
//  ConfirmTransferInteractorImpl.swift
//  ConfirmTransfer
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import Core
import Profile

class ConfirmTransferInteractorImpl: ConfirmTransferInteractor {
    
    var interactorOutput: ConfirmTransferInteractorOutput?
    let profileNetworkManager: ProfileNetworkManager
    let balanceNetworkManager: BalanceNetworkManager
    
    init(profileNetworkManager: ProfileNetworkManager, balanceNetworkManager: BalanceNetworkManager) {
        self.profileNetworkManager = profileNetworkManager
        self.balanceNetworkManager = balanceNetworkManager
    }
    
    func getContact() {
    }
    
    func getBalance() {
        self.balanceNetworkManager.getBalance { (data, error) in
            if let balance = data {
                let amount = BalanceUserEntity(balance: balance.balance)
                self.interactorOutput?.loadedBalance(balance: amount)
            }
        }
    }
}
