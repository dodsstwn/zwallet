//
//  ConfirmTransferInteractor.swift
//  ConfirmTransfer
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation

protocol ConfirmTransferInteractor {
    func getContact()
    func getBalance()
}
