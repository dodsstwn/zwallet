//
//  ConfirmTransferRouterImpl.swift
//  ConfirmTransfer
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import UIKit
import Core

public class ConfirmTransferRouterImpl {
    
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "id.mandiri.dip.ConfirmTransfer")
        let vc = ConfirmTransferViewController(nibName: "ConfirmTransferViewController", bundle: bundle)
        let profileNetworkManager = ProfileNetworkManagerImpl()
        let balanceNetworkManager = BalanceNetworkManagerImpl()
        let router = ConfirmTransferRouterImpl()
        let interactor = ConfirmTransferInteractorImpl(profileNetworkManager: profileNetworkManager, balanceNetworkManager: balanceNetworkManager)
        let presenter = ConfirmTransferPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension ConfirmTransferRouterImpl: ConfirmTransferRouter {
    func navigateToTransfer() {
        AppRouter.shared.navigateToTransfer()
    }
    
    func navigateToPIN() {
        AppRouter.shared.navigateToTransferPIN()
    }
}
