//
//  ConfirmTransferRouter.swift
//  ConfirmTransfer
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import UIKit

protocol ConfirmTransferRouter {
    func navigateToTransfer()
    func navigateToPIN()
}
