//
//  ConfirmTransferView.swift
//  ConfirmTransfer
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import Core

protocol ConfirmTransferView {
    func showDataContact(contact: NewUserEntity)
    func showDataBalance(balance: BalanceUserEntity)
}

