//
//  ConfirmTransferViewController.swift
//  ConfirmTransfer
//
//  Created by Dodsstwn on 30/05/21.
//

import UIKit
import Core
import Kingfisher

class ConfirmTransferViewController: UIViewController {
    
    var presenter: ConfirmTransferPresenter?
    
    @IBAction func backButton(_ sender: Any) {
        self.presenter?.backToTransfer()
    }
    
    
    @IBAction func continueButton(_ sender: Any) {
        self.presenter?.goToTransferPIN()
        UserDefaultHelper.shared.set(key: .userBalance, value: balanceLeft.text)
    }
    
    // Top View
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var userPhone: UILabel!
    
    // Body View
    @IBOutlet weak var amountTxt: UILabel!
    @IBOutlet weak var balanceLeft: UILabel!
    @IBOutlet weak var dateTxt: UILabel!
    @IBOutlet weak var timeTxt: UILabel!
    @IBOutlet weak var notesTxt: UILabel!
    
    let image: String? = UserDefaultHelper.shared.get(key: .userImageUrl)
    let name: String? = UserDefaultHelper.shared.get(key: .userName)
    let phone: String? = UserDefaultHelper.shared.get(key: .userPhone)
    let amount: String? = UserDefaultHelper.shared.get(key: .amountTransaction)
    let date: String? = UserDefaultHelper.shared.get(key: .dateTransaction)
    let time: String? = UserDefaultHelper.shared.get(key: .timeTransaction)
    let notes: String? = UserDefaultHelper.shared.get(key: .notesTransaction)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.loadBalanceData()
        // Top
        let url = URL(string: "\(image ?? "")")
        imageUser.kf.setImage(with: url)
        contactName.text = name ?? ""
        userPhone.text = phone ?? ""
        
        // Body
        let tempAmountInt: Int = Int(amount ?? "")!
        amountTxt.text = tempAmountInt.formatToIdr()
        dateTxt.text = date ?? ""
        timeTxt.text = time ?? ""
        notesTxt.text = notes ?? ""
    }
}

extension ConfirmTransferViewController: ConfirmTransferView {
    func showDataContact(contact: NewUserEntity) {
    }
    
    func showDataBalance(balance: BalanceUserEntity) {
        let balanceUser = balance.balance
        let totalBalanceLeft: Int = Int(balanceUser) - Int(amount ?? "")!
        balanceLeft.text = totalBalanceLeft.formatToIdr()
    }
}
