//
//  ContactPresenterImpl.swift
//  Contact
//
//  Created by Dodsstwn on 29/05/21.
//

import Foundation
import Core

class ContactPresenterImpl: ContactPresenter {
    
    let view: ContactView
    let interactor: ContactInteractor
    let router: ContactRouter
    
    init(view: ContactView, interactor: ContactInteractor, router: ContactRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadContact() {
        self.interactor.getContact()
    }
    
    func showHome() {
        self.router.navigateToHome()
    }
    
    func goToTransfer() {
        self.router.navigateToTransfer()
    }
}

extension ContactPresenterImpl: ContactInteractorOutput {
    func loadedData(contacts: [ContactProfileEntity]) {
        self.view.showContactData(contacts: contacts)
    }
}
