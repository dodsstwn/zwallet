//
//  ContactPresenter.swift
//  Contact
//
//  Created by Dodsstwn on 29/05/21.
//

import Foundation
import UIKit

protocol ContactPresenter {
    func loadContact()
    func showHome()
    func goToTransfer()
}
