//
//  ContactTableViewCell.swift
//  Contact
//
//  Created by Dodsstwn on 28/05/21.
//

import UIKit
import Kingfisher

class ContactTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var contactPhone: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func showData(profile: ContactProfileEntity) {
        self.contactName.text = profile.name
        self.contactPhone.text = profile.phone
        let url = URL(string: profile.image)
        self.imageUser.kf.setImage(with: url)
    }
    
}
