//
//  ContactRouter.swift
//  Contact
//
//  Created by Dodsstwn on 29/05/21.
//

import Foundation
import UIKit

protocol ContactRouter {
    func navigateToHome()
    func navigateToTransfer()
}
