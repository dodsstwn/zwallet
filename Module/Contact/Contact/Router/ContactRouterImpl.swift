//
//  ContactRouterImpl.swift
//  Contact
//
//  Created by Dodsstwn on 29/05/21.
//

import Foundation
import UIKit
import Core

public class ContactRouterImpl {
    
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "id.mandiri.dip.Contact")
        let vc = ContactViewController(nibName: "ContactViewController", bundle: bundle)
        let profileNetworkManager = ProfileNetworkManagerImpl()
        let router = ContactRouterImpl()
        let interactor = ContactInteractorImpl(profileNetworkManager: profileNetworkManager)
        let presenter = ContactPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension ContactRouterImpl: ContactRouter {
    func navigateToHome() {
        AppRouter.shared.navigateToHome()
    }
    
    func navigateToTransfer() {
        AppRouter.shared.navigateToTransfer()
    }
}
