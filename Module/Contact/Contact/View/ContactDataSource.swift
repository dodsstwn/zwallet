//
//  ContactDataSource.swift
//  Contact
//
//  Created by Dodsstwn on 29/05/21.
//

import Foundation
import UIKit
import Core
import Kingfisher
import Transfer

class ContactDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var viewController: ContactViewController!
    var contacts: [ContactProfileEntity] = []
    var router: ContactRouter!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath) as! ContactTableViewCell
        cell.contactName.text = contacts[indexPath.row].name
        cell.contactPhone.text = contacts[indexPath.row].phone
        let url = URL(string: contacts[indexPath.row].image)
        cell.imageUser.kf.setImage(with: url)
        
        return cell
    }    
}
