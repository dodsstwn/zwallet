//
//  ContactViewController.swift
//  Contact
//
//  Created by Dodsstwn on 28/05/21.
//

import UIKit
import Core
import Transfer

class ContactViewController: UIViewController, UITableViewDelegate {

    var dataSource = ContactDataSource()
    var presenter: ContactPresenter?
    var contacts: [ContactProfileEntity] = []
    var router: ContactRouter!
    
    @IBAction func backButton(_ sender: Any) {
        self.presenter?.showHome()
    }
    
    @IBAction func searchButton(_ sender: Any) {
    }
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalContactText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.loadContact()
        
        let email: String? = UserDefaultHelper.shared.get(key: .userEmail)
        
        // old setup
        self.setupTableView()
        
        // print UserDefault
        print(">> \(email ?? "")")
        
        // cek isi contacts
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            print(">> \(self.dataSource.contacts.count)")
            self.totalContactText.text = "\(self.dataSource.contacts.count) Contacts Found"
        }
    }
    
    func setupTableView() {
        self.dataSource.viewController = self
        self.tableView.register(UINib(nibName: "ContactTableViewCell", bundle: Bundle(identifier: "id.mandiri.dip.Contact")), forCellReuseIdentifier: "ContactTableViewCell")
        self.tableView.dataSource = self.dataSource
        self.tableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 100
        default:
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath) as! ContactTableViewCell
        cell.contactName.text = contacts[indexPath.row].name
        cell.contactPhone.text = contacts[indexPath.row].phone
        let url = URL(string: contacts[indexPath.row].image)
        cell.imageUser.kf.setImage(with: url)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Click on Each Cell (Receiver Contact)
        let tempArray: Array = dataSource.contacts
        print(">> Name: \(tempArray[indexPath.row].name)")
        print(">> ID: \(tempArray[indexPath.row].id)")
        
        UserDefaultHelper.shared.set(key: .userID, value: dataSource.contacts[indexPath.row].id)
        UserDefaultHelper.shared.set(key: .userName, value: dataSource.contacts[indexPath.row].name)
        UserDefaultHelper.shared.set(key: .userPhone, value: dataSource.contacts[indexPath.row].phone)
        UserDefaultHelper.shared.set(key: .userImageUrl, value: dataSource.contacts[indexPath.row].image)
        
        self.presenter?.goToTransfer()
    }
}

extension ContactViewController: ContactView {
    func showContactData(contacts: [ContactProfileEntity]) {
        self.dataSource.contacts = contacts
        self.tableView.reloadData()
    }
}
