//
//  ContactView.swift
//  Contact
//
//  Created by Dodsstwn on 28/05/21.
//

import Foundation
import Core

protocol ContactView {
    func showContactData(contacts: [ContactProfileEntity])
}
