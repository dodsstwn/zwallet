//
//  ContactInteractorImpl.swift
//  Contact
//
//  Created by Dodsstwn on 29/05/21.
//

import Foundation
import Core
import Profile

class ContactInteractorImpl: ContactInteractor {
    
    var interactorOutput: ContactInteractorOutput?
    let profileNetworkManager: ProfileNetworkManager
    
    init(profileNetworkManager: ProfileNetworkManager) {
        self.profileNetworkManager = profileNetworkManager
    }
    
    func getContact() {
        self.profileNetworkManager.getContact { (data, error) in
            var contacts: [ContactProfileEntity] = []
            
            data?.forEach({ (invoiceData) in
                contacts.append(ContactProfileEntity(id: invoiceData.id, name: "\(invoiceData.name)", phone: invoiceData.phone, image: "\(AppConstant.baseUrl)\(invoiceData.image)"))
                self.interactorOutput?.loadedData(contacts: contacts)
            })
        }
    }
}
