//
//  ContactInteractorOutput.swift
//  Contact
//
//  Created by Dodsstwn on 29/05/21.
//

import Foundation
import Core

protocol ContactInteractorOutput {
    func loadedData(contacts: [ContactProfileEntity])
}
