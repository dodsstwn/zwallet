//
//  ContactInteractor.swift
//  Contact
//
//  Created by Dodsstwn on 29/05/21.
//

import Foundation

protocol ContactInteractor {
    func getContact()
}
