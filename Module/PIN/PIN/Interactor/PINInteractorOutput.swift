//
//  PINInteractorOutput.swift
//  PIN
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation

protocol PINInteractorOutput {
    func authenticationResult(isSuccess: Bool)
}
