//
//  PINInteractor.swift
//  PIN
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation

protocol PINInteractor {
    func postPINData(pin: String)
}
