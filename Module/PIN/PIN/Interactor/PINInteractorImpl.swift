//
//  PINInteractorImpl.swift
//  PIN
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation
import Core

class PINInteractorImpl: PINInteractor {
    
    var interactorOutput: PINInteractorOutput?
    var authNetworkManager: AuthNetworkManager
    
    init(networkManager: AuthNetworkManager) {
        self.authNetworkManager = networkManager
    }
    
    func postPINData(pin: String) {
        self.authNetworkManager.pin(pin: pin) { data, error in
            if data?.status == 200 {
                self.interactorOutput?.authenticationResult(isSuccess: true)
            } else {
                self.interactorOutput?.authenticationResult(isSuccess: false)
            }
        }
    }
}
