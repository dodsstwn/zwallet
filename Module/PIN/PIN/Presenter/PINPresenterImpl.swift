//
//  PINPresenterImpl.swift
//  PIN
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation

class PINPresenterImpl: PINPresenter {
    
    let view: PINView
    let interactor: PINInteractor
    let router: PINRouter
    
    init(view: PINView, interactor: PINInteractor, router: PINRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func pin(pin: String) {
        self.interactor.postPINData(pin: pin)
    }
    func backToLogin() {
        self.router.navigateToLogin()
    }
}

extension PINPresenterImpl: PINInteractorOutput {
    func authenticationResult(isSuccess: Bool) {
        if isSuccess {
            self.router.navigateToHome()
        } else {
            self.view.showError()
        }
    }
}
