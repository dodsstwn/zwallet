//
//  PINRouterImpl.swift
//  PIN
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation
import UIKit
import Core

public class PINRouterImpl {
    
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "id.mandiri.dip.PIN")
        let vc = PINViewController(nibName: "PINViewController", bundle: bundle)
        let networkManager = AuthNetworkManagerImpl()
        let router = PINRouterImpl()
        let interactor = PINInteractorImpl(networkManager: networkManager)
        let presenter = PINPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension PINRouterImpl: PINRouter {
    
    func navigateToHome() {
        AppRouter.shared.navigateLogin()
    }
    
    func navigateToPIN() {
        AppRouter.shared.navigateToPIN()
    }
    
    func navigateToLogin() {
        AppRouter.shared.navigateLogin()
    }
}
