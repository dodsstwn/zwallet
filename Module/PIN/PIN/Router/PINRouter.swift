//
//  PINRouter.swift
//  PIN
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation

protocol PINRouter {
    func navigateToHome()
    func navigateToPIN()
    func navigateToLogin()
}
