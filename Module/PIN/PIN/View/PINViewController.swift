//
//  PINViewController.swift
//  PIN
//
//  Created by Dodsstwn on 27/05/21.
//

import UIKit
import Core

class PINViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var firstTxt: UITextField!
    @IBOutlet weak var secondTxt: UITextField!
    @IBOutlet weak var thirdTxt: UITextField!
    @IBOutlet weak var fourthTxt: UITextField!
    @IBOutlet weak var fifthTxt: UITextField!
    @IBOutlet weak var sixTxt: UITextField!
    
    var presenter: PINPresenter?
    
    @IBOutlet weak var successView: UIView!
    @IBAction func loginNowButton(_ sender: Any) {
        self.presenter?.backToLogin()
    }
    
    @IBAction func confirmButton(_ sender: Any) {
        
        let pinStr: String = "\(firstTxt.text ?? "")\(secondTxt.text ?? "")\(thirdTxt.text ?? "")\(fourthTxt.text ?? "")\(fifthTxt.text ?? "")\(sixTxt.text ?? "")"
        self.presenter?.pin(pin: pinStr)
        self.successView.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Text Field
        firstTxt.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        firstTxt.delegate = self
        secondTxt.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        secondTxt.delegate = self
        thirdTxt.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        thirdTxt.delegate = self
        fourthTxt.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        fourthTxt.delegate = self
        fifthTxt.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        fifthTxt.delegate = self
        sixTxt.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        sixTxt.delegate = self
        
        // Success View
        self.successView.isHidden = true
        
        // After 50s doesn't input the OTP back to Login Page
        DispatchQueue.main.asyncAfter(deadline: .now() + 50.0) {
            self.presenter?.backToLogin()
        }
    }
    
    let maxLength: Int = 1
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if range.location > maxLength - 1 {
            firstTxt.text?.removeLast()
            secondTxt.text?.removeLast()
            thirdTxt.text?.removeLast()
            fourthTxt.text?.removeLast()
            fifthTxt.text?.removeLast()
            sixTxt.text?.removeLast()
        }
        return true
        }
}

extension PINViewController: PINView {
    func showError() {
        let alert = UIAlertController(
            title: "PIN Message!", message: "Your PIN is succeed to set!", preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
