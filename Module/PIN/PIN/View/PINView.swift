//
//  PINView.swift
//  PIN
//
//  Created by Dodsstwn on 27/05/21.
//

import Foundation

protocol PINView {
    func showError()
}
