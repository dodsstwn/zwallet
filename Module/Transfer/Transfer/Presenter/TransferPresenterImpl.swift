//
//  TransferPresenterImpl.swift
//  Transfer
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import Core

class TransferPresenterImpl: TransferPresenter {
    
    let view: TransferView
    let interactor: TransferInteractor
    let router: TransferRouter
    
    init(view: TransferView, interactor: TransferInteractor, router: TransferRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadContactData() {
        self.router.navigateToDetailTransfer()
    }
    
    func loadBalanceData() {
        self.interactor.getBalance()
    }
}

extension TransferPresenterImpl: TransferInteractorOutput {
    func loadedBalance(balance: BalanceUserEntity) {
        self.view.showBalance(balance: balance)
    }
}
