//
//  TransferPresenter.swift
//  Transfer
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import UIKit

protocol TransferPresenter {
    func loadContactData()
    func loadBalanceData()
}
