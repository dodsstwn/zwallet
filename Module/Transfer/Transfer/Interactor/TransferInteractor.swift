//
//  TransferInteractor.swift
//  Transfer
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation

protocol TransferInteractor {
    func getContact()
    func getBalance()
}
