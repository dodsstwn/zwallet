//
//  TransferInteractorOutput.swift
//  Transfer
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import Core

protocol TransferInteractorOutput {
    func loadedBalance(balance: BalanceUserEntity)
}
