//
//  TransferRouterImpl.swift
//  Transfer
//
//  Created by Dodsstwn on 30/05/21.
//

import Foundation
import UIKit
import Core

public class TransferRouterImpl {
    
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "id.mandiri.dip.Transfer")
        let vc = TransferViewController(nibName: "TransferViewController", bundle: bundle)
        let profileNetworkManager = ProfileNetworkManagerImpl()
        let balanceNetworkManager = BalanceNetworkManagerImpl()
        let router = TransferRouterImpl()
        let interactor = TransferInteractorImpl(profileNetworkManager: profileNetworkManager, balanceNetworkManager: balanceNetworkManager)
        let presenter = TransferPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension TransferRouterImpl: TransferRouter {
    func navigateToContact() {
        AppRouter.shared.navigateToContact()
    }
    
    func navigateToDetailTransfer() {
        // routing to transferDetail
    }
}
