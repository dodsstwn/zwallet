//
//  TransferView.swift
//  Transfer
//
//  Created by Dodsstwn on 29/05/21.
//

import Foundation
import Core

protocol TransferView {
    func showBalance(balance: BalanceUserEntity)
}
