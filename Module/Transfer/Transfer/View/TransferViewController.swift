//
//  TransferViewController.swift
//  Transfer
//
//  Created by Dodsstwn on 29/05/21.
//

import UIKit
import Kingfisher
import Core

public class TransferViewController: UIViewController {
    
    let name: String? = UserDefaultHelper.shared.get(key: .userName)
    let phone: String? = UserDefaultHelper.shared.get(key: .userPhone)
    let image: String? = UserDefaultHelper.shared.get(key: .userImageUrl)
    
    var presenter: TransferPresenter?
    
    @IBAction func backButton(_ sender: Any) {
        AppRouter.shared.navigateToContact()
    }
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameTxt: UILabel!
    @IBOutlet weak var userPhoneTxt: UILabel!
    @IBOutlet weak var amountTxtField: UITextField!
    @IBOutlet weak var userAmount: UILabel!
    @IBOutlet weak var notesTxtField: UITextField!
    
    @IBAction func continueButton(_ sender: Any) {
        UserDefaultHelper.shared.set(key: .dateTransaction, value: dateConverter())
        UserDefaultHelper.shared.set(key: .timeTransaction, value: timeConverter())
        UserDefaultHelper.shared.set(key: .amountTransaction, value: amountTxtField.text)
        UserDefaultHelper.shared.set(key: .notesTransaction, value: notesTxtField.text)
        
        // Should be move
        AppRouter.shared.navigateToConfirmTransfer()
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.loadBalanceData()
        
        // For Testing on Terminal
        print(">> \(name ?? "")")
        print(">> \(phone ?? "")")
        print(">> \(image ?? "")")
        
        userNameTxt.text = name ?? ""
        userPhoneTxt.text = phone ?? ""
        let url = URL(string: "\(image ?? "")")
        userImage.kf.setImage(with: url)
    }
}

extension TransferViewController: TransferView {
    func showBalance(balance: BalanceUserEntity) {
        userAmount.text = balance.balance.formatToIdr()
    }
}
