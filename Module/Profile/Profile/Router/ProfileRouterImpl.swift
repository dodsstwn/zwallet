//
//  ProfileRouterImpl.swift
//  Profile
//
//  Created by Dodsstwn on 28/05/21.
//

import Foundation
import UIKit
import Core

public class ProfileRouterImpl {
    
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "id.mandiri.dip.Profile")
        let vc = ProfileViewController(nibName: "ProfileViewController", bundle: bundle)
        
//        let profileNetworkManager = ProfileNetworkManagerImpl()
        let balanceNetworkManager = BalanceNetworkManagerImpl()
        
        let router = ProfileRouterImpl()
        let interactor = ProfileInteractorImpl(networkManager: balanceNetworkManager)
        let presenter = ProfilePresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension ProfileRouterImpl: ProfileRouter {
    func navigateToHome() {
        NotificationCenter.default.post(name: Notification.Name("reloadRootView"), object: nil)
    }
    
    func navigateToLogin() {
        AppRouter.shared.navigateLogin()
    }
}
