//
//  ProfileViewController.swift
//  Profile
//
//  Created by Dodsstwn on 28/05/21.
//

import UIKit
import Kingfisher
import Core

class ProfileViewController: UIViewController {

    var presenter: ProfilePresenterImpl?
    var interactor: ProfileInteractor?
    var userProfile: ProfileEntity?
    
    @IBOutlet weak var imageProfile: UIImageView!
    
    @IBAction func editIcon(_ sender: Any) {
    }
    @IBAction func textEdit(_ sender: Any) {
    }
    @IBAction func backButton(_ sender: Any) {
        AppRouter.shared.navigateToHome()
    }
    
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var phoneProfile: UILabel!
    
    
    @IBAction func textPersonalInfo(_ sender: Any) {
    }
    @IBAction func buttonPersonalInfo(_ sender: Any) {
    }
    
    @IBAction func textChangePassword(_ sender: Any) {
    }
    @IBAction func buttonChangePassword(_ sender: Any) {
    }
    
    @IBAction func textChangePIN(_ sender: Any) {
    }
    @IBAction func buttonChangePIN(_ sender: Any) {
    }
    
    @IBAction func logoutButton(_ sender: Any) {
        self.presenter?.logout()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.loadProfile()
    }
}

extension ProfileViewController: ProfileView {
    func showUserProfileData(userProfile: NewUserEntity) {
        let url = URL(string: "\(AppConstant.baseUrl)\(userProfile.imageUrl)")
        self.imageProfile.kf.setImage(with: url)
        self.profileName.text = "\(userProfile.name)"
        self.phoneProfile.text = userProfile.phone
    }
}
