//
//  ProfileEntity.swift
//  Profile
//
//  Created by Dodsstwn on 28/05/21.
//

import Foundation

struct ProfileEntity {
    var firstname: String
    var lastname: String
    var email: String
    var phone: String
    var imageUrl: String
}

var profile: [ProfileEntity] = []
