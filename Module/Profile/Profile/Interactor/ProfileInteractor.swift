//
//  ProfileInteractor.swift
//  Profile
//
//  Created by Dodsstwn on 28/05/21.
//

import Foundation

protocol ProfileInteractor {
    func postProfileData()
}
