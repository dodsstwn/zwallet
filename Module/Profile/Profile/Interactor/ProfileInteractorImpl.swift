//
//  ProfileInteractorImpl.swift
//  Profile
//
//  Created by Dodsstwn on 28/05/21.
//

import Foundation
import Core

class ProfileInteractorImpl: ProfileInteractor {
    
    var interactorOutput: ProfileInteractorOutput?
    let balanceNetworkManager: BalanceNetworkManager
    
    init(networkManager: BalanceNetworkManager) {
        self.balanceNetworkManager = networkManager
    }
    
    func postProfileData() {
        self.balanceNetworkManager.getBalance { (data, error) in
            if let dataProfile = data {

                let userProfile = NewUserEntity(name: dataProfile.name, phone: dataProfile.phone, balance: dataProfile.balance, imageUrl: dataProfile.image)
                
                self.interactorOutput?.loadedUserProfileData(userProfile: userProfile)
                
            }
        }
    }
}
