//
//  ProfileInteractorOutput.swift
//  Profile
//
//  Created by Dodsstwn on 28/05/21.
//

import Foundation
import Core

protocol ProfileInteractorOutput {
    func loadedUserProfileData(userProfile: NewUserEntity)
}
