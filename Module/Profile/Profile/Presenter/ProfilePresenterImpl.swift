//
//  ProfilePresenterImpl.swift
//  Profile
//
//  Created by Dodsstwn on 28/05/21.
//

import Foundation
import UIKit
import Core

class ProfilePresenterImpl: ProfilePresenter {

    let view: ProfileView
    let interactor: ProfileInteractor
    let router: ProfileRouter
    
    init(view: ProfileView, interactor: ProfileInteractor, router: ProfileRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadProfile() {
        print(">>>>> loadProfile")
        self.interactor.postProfileData()
    }
    
    func logout() {
        UserDefaultHelper.shared.remove(key: .userToken)
        self.router.navigateToLogin()
    }
    
}

extension ProfilePresenterImpl: ProfileInteractorOutput {
    func loadedUserProfileData(userProfile: NewUserEntity) {
        self.view.showUserProfileData(userProfile: userProfile)
    }
}
