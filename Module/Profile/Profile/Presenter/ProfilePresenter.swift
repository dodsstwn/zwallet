//
//  ProfilePresenter.swift
//  Profile
//
//  Created by Dodsstwn on 28/05/21.
//

import Foundation

protocol ProfilePresenter {
    func loadProfile()
    func logout()
}
