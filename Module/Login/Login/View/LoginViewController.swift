//
//  LoginViewController.swift
//  Login
//
//  Created by Dodsstwn on 24/05/21.
//

import UIKit
import Core

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    var presenter: LoginPresenter?
    
    @IBAction func signUpButton(_ sender: Any) {
        self.presenter?.showSignUp()
        print("> All Access!")
    }
    
    var iconClick = true
    @IBAction func showPassButton(_ sender: Any) {
        if (iconClick == true) {
            passwordText.isSecureTextEntry = false
        } else {
            passwordText.isSecureTextEntry = true
        }
        
        iconClick = !iconClick
    }
    
    @IBOutlet weak var signInButtonOutlet: UIButton!
    @IBAction func loginButton(_ sender: Any) {
        
        let email: String = emailText.text ?? ""
        UserDefaultHelper.shared.set(key: .userEmail, value: email)
        let password: String = passwordText.text ?? ""
        
        self.presenter?.login(email: email, password: password)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

extension LoginViewController: LoginView {
    func showError() {
        let alert = UIAlertController(
            title: "Peringatan",
            message: "Username atau password anda salah, silahkan coba lagi",
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

