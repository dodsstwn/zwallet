//
//  LoginPresenterImpl.swift
//  Login
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation
import Core

class LoginPresenterImpl: LoginPresenter {
    
    let view: LoginView
    let interactor: LoginInteractor
    let router: LoginRouter
    
    init(view: LoginView, interactor: LoginInteractor, router: LoginRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func login(email: String, password: String) {
        self.interactor.postLoginData(email: email, password: password)
    }
    
    func showSignUp() {
        self.router.navigateToRegister()
    }
}

extension LoginPresenterImpl: LoginInteractorOutput {
    func authenticationResult(isSuccess: Bool, status: Int, dataHasPin: Bool) {
        if isSuccess {
            if status == 200 && dataHasPin == true {
                self.router.navigateToHome()
            } else if status == 200 && dataHasPin == false {
                self.router.navigateToPIN()
            }
        } else {
            self.view.showError()
        }
    }
}
