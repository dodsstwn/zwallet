//
//  LoginPresenter.swift
//  Login
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation

protocol LoginPresenter {
    func login(email: String, password: String)
    func showSignUp()
}
