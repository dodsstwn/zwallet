//
//  LoginInteractorImpl.swift
//  Login
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation
import Core

class LoginInteractorImpl: LoginInteractor {
    
    
    var interactorOutput: LoginInteractorOutput?
    let authNetworkManager: AuthNetworkManager
    
    init(networkManager: AuthNetworkManager) {
        self.authNetworkManager = networkManager
    }
    
    func postLoginData(email: String, password: String) {
        self.authNetworkManager.login(email: email, password: password) { data, error in
            if let loginData = data?.data {
                UserDefaultHelper.shared.set(key: .userToken, value: loginData.token)
                self.interactorOutput?.authenticationResult(isSuccess: true, status: data?.status ?? 0, dataHasPin: data?.data.hasPin ?? false)
            } else {
                self.interactorOutput?.authenticationResult(isSuccess: false, status: data?.status ?? 0, dataHasPin: data?.data.hasPin ?? false)
            }
        }
    }
}
