//
//  LoginInteractorOutpur.swift
//  Login
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation

protocol LoginInteractorOutput {
    func authenticationResult(isSuccess: Bool, status: Int, dataHasPin: Bool)
}
