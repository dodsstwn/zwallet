//
//  LoginRouter.swift
//  Login
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation

protocol LoginRouter {
    func navigateToHome()
    func navigateToRegister()
    func navigateToPIN()
}
