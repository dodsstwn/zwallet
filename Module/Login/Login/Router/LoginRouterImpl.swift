//
//  LoginRouterImpl.swift
//  Login
//
//  Created by Dodsstwn on 24/05/21.
//

import Foundation
import UIKit
import Core

public class LoginRouterImpl {
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "id.mandiri.dip.Login")
        let vc = LoginViewController(nibName: "LoginViewController", bundle: bundle)
        let networkManager = AuthNetworkManagerImpl()
        let router = LoginRouterImpl()
        let interactor = LoginInteractorImpl(networkManager: networkManager)
        let presenter = LoginPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension LoginRouterImpl: LoginRouter {
    
    func navigateToHome() {
        NotificationCenter.default.post(name: Notification.Name("reloadRootView"), object: nil)
    }
    
    func navigateToRegister() {
        AppRouter.shared.navigateToRegister()
    }
    
    func navigateToPIN() {
        AppRouter.shared.navigateToPIN()
    }
}
