# ZWallet
ZWallet is an e-wallet application that has the main feature where users can transfer balances and receive balances. It also has a main security feature with pin authentication when entering the main page or when the user wants to transfer balances.

## Launchscreen and Homepage

![launchscreen.png](launchscreen.png) ![homepage.png](homepage.png)

## Profile and Transfer Confirmation

![profile.png](profile.png) ![transfer_confirmation.png](transfer_confirmation.png)

## Features
- [x] Login
- [x] Register
- [x] Home
- [x] See all transaction
- [x] Transfer
- [x] Profile

## Requirement
- iOS 12.1++
- Xcode 12.4

## Installation
### GIT Clone or Download from GIT
```
https://gitlab.com/dodsstwn/zwallet
```

```
1. Download zip from https://gitlab.com/dodsstwn/zwallet
2. Extract zip
```

## CocoaPods
You can use CocoaPods to install the library by adding it to your Apps.

```
platform :ios, '11.0'
use_frameworks!
pod 'Kingfisher'
```
```
platform :ios, '11.0'
use_frameworks!
pod 'netfox'
```
```
platform :ios, '11.0'
use_frameworks!
pod 'Moya'
```

## Usage Example

### Run App
1. Go to /App folder
2. Run code below
```
pod install
```
3. Open App > ZWallet.xcworkspace
4. Set your simulator and run your project.
5. Your apps is ready to use!

### Usage details
ZWallet apps is using VIPER for them design pattern, consist of View, Interactor, Presenter, Entity, and Router. VIPER dividing into various layer which have their respective function.

## Meta
Dody Setiawan - Digital Innovation Program Batch 1 by Bank Mandiri
dodysetiawan6690@gmail.com
